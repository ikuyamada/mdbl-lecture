# -*- coding: utf-8 -*-

import numpy as np
import os.path
import pickle
from collections import Counter
from skimage.io import imread
from skimage.color import rgb2hsv
from sklearn.cluster import KMeans
from sklearn.preprocessing import normalize

import art_image

KMEANS_PICKLE_FILE_NAME = 'color_kmeans_model.pickle'
NUM_DIMENTIONS = 32
NUM_SAMPLES = 1500


def get_color_vector(image_file):
    pixels = rgb2hsv(imread(image_file))
    (w, h, _, ) = pixels.shape
    pixels = pixels.reshape(w * h, 3)

    model = get_kmeans_model()
    labels = model.predict(pixels)

    counter = Counter(labels)
    keys = range(NUM_DIMENTIONS)
    vector = normalize([[float(counter[k]) for k in keys]])[0]

    return dict([('color' + str(k), vector[i]) for (i, k) in enumerate(keys)])


def get_color_list():
    return ('color' + str(n) for n in range(NUM_DIMENTIONS))


def construct_color_matrix():
    c = 0
    matrix = np.ndarray(shape=(0, 3))
    for image in art_image.ArtImage.find():
        if c == NUM_SAMPLES:
            break
        c += 1

        img_arr = rgb2hsv(imread(image.path))
        img_arr = img_arr.reshape(img_arr.shape[0] * img_arr.shape[1], img_arr.shape[2])
        matrix = np.concatenate((matrix, img_arr))

    return matrix


def get_kmeans_model(cache=True):
    if cache and os.path.isfile(KMEANS_PICKLE_FILE_NAME):
        with open(KMEANS_PICKLE_FILE_NAME) as f:
            return pickle.load(f)

    model = KMeans(n_clusters=NUM_DIMENTIONS, random_state=0)
    X = construct_color_matrix()
    model.fit(X)

    with open(KMEANS_PICKLE_FILE_NAME, 'w') as f:
        pickle.dump(model, f)

    return model
