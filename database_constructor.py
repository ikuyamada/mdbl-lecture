# -*- coding: utf-8 -*-

import requests
import time
from BeautifulSoup import BeautifulSoup
from redis_cache import cache_it_json
import logging

from art_image import ArtImage
import database

BASE_URL = 'https://images.nga.gov'
SEARCH_BASE_URL = BASE_URL + '/en/search/do_advanced_search.html?form_name=default&all_words=&exact_phrase=&exclude_words=&artist_last_name=&keywords_in_title=&accession_number=&school=&Classification=&medium=&year=&year2=&open_access=1&qw=&grid_layout=3'


def construct_database():
    page = 0

    db = database.connection()
    _define_inner_product_function()

    while True:
        print 'Page:', page
        page += 1

        html = _get_list_html(page)

        soup = BeautifulSoup(html)

        img_a_tags = soup.findAll('a', {'class': 'imageLink'})
        if not img_a_tags:
            break

        for img_link in img_a_tags:
            img_node = img_link.find('img')
            title = img_node['title'],
            url = BASE_URL + img_node['src']
            ins = ArtImage(title, url)
            try:
                ins.create_cache()
            except:
                logging.warn('Unable to create cache image: %s', url)

            try:
                ins.save()
            except:
                logging.warn('Unable to create database entry: %s', url)

    db['art_image'].create_index('url')
    db['art_image'].create_index('name')


def _define_inner_product_function():
    db = database.connection()
    dim = 32
    arg_list = ', '.join(['float'] * (dim * 2))
    db.query('DROP FUNCTION IF EXISTS inner_product'+ str(dim) + '(' + arg_list + ');')

    inner_query = 'SELECT ' + ' + '.join(['$%d * $%d' % (n, n + dim) for n in range(1, dim+1)]) + ' AS inner_product';
    db.query('CREATE FUNCTION inner_product' + str(dim)+'(' + arg_list + ') '
             'RETURNS float AS \'' + inner_query + '\' LANGUAGE \'sql\'')


@cache_it_json(limit=1000, expire=60*60*24*365)
def _get_list_html(page=0):
    time.sleep(1)
    return requests.get(SEARCH_BASE_URL, params=dict(page=page)).text
