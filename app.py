# -*- coding: utf-8 -*-

from flask import Flask, request, render_template
from PIL import Image

from art_image import ArtImage

app = Flask(__name__)


@app.route("/")
def list():
    query = request.args.get('q')
    if query:
        vec = ArtImage.find_one(name=query).color_vector
    else:
        vec = None

    c = 0
    images = []
    for art_image in ArtImage.find(vec=vec):
        c += 1
        if c == 1000:
            break

        try:
            (width, height) = Image.open(art_image.path).size
        except:
            continue

        obj = dict(
            src=art_image.path,
            title=art_image.title,
            name=art_image.name,
            width=width,
            height=height,
        )
        images.append(obj)

    return render_template('list.html', images=images)


if __name__ == "__main__":
    app.run(debug=True)
