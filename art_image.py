# -*- coding: utf-8 -*-

import time
import os.path
import requests

import color_vector
import database

IMAGE_CACHE_PATH = 'static/images'


class ArtImage(object):
    def __init__(self, title, url):
        self._title = title
        self._url = url

    @property
    def title(self):
        return self._title

    @property
    def url(self):
        return self._url

    @property
    def name(self):
        return self._url.split('/')[-1]

    @property
    def content(self):
        with open(self.path) as f:
            return f.read()

    @property
    def path(self):
        file_path = '%s/%s' % (IMAGE_CACHE_PATH, self.name)
        return file_path

    @property
    def color_vector(self):
        return color_vector.get_color_vector(self.path)

    def save(self):
        obj = self.color_vector
        obj['title'] = self.title
        obj['url'] = self.url
        obj['name'] = self.name

        db = database.connection()
        db['art_image'].upsert(obj, ['url'])

    def create_cache(self):
        if os.path.isfile(self.path):
            return

        r = requests.get(self._url, stream=True)
        time.sleep(1)
        assert r.status_code == 200

        print 'Requesting:', self._url

        with open(self.path, 'wb') as f:
            for chunk in r.iter_content():
                f.write(chunk)

    @staticmethod
    def load(obj):
        return ArtImage(obj['title'], obj['url'])

    @classmethod
    def find(cls, vec=None, **kwargs):
        db = database.connection()
        if vec:
            score_q = 'inner_product32(' + ', '.join(
                    ['art_image.' + c for c in color_vector.get_color_list()] +
                    [str(vec[c]) for c in color_vector.get_color_list()]
                ) + ') AS score'
            q = 'SELECT title, url, ' + score_q + ' FROM art_image ORDER BY score DESC NULLS LAST'
            query_obj = db.query(q)

        else:
            query_obj = db['art_image'].find(**kwargs)

        for obj in query_obj:
            yield ArtImage.load(obj)

    @classmethod
    def find_one(cls, vec=None, **kwargs):
        return cls.find(vec, **kwargs).next()
